package com.example.student.redditpa;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

public class TodoActivity extends SingleFragmentActivity implements ActivityCallback {

    public ArrayList<com.example.student.redditpa.TodoItem> TodoItem = new ArrayList<>();
    public int currentItem;
    private ViewPager viewPager;

    @Override
    protected Fragment createFragment() {
        return new TodoListFragment();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_fragment;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewPager = (ViewPager) findViewById(R.id.viewpager);

//        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
//        viewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
//            @Override
//            public android.support.v4.app.Fragment getItem(int position) {
//                return TodoActivity.newFragment(position, TodoItem);
//            }
//
//            @Override
//            public int getCount() {
//                return TodoItem.size();
//            }
//        });

//        for (int index = 0; index < TodoItem.size(); index++) {
//            if (TodoItem.get(index).equals(TodoItem.toString())) {
//                viewPager.setCurrentItem(index);
//                break;
//            }
//        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPostSelected(int pos, ArrayList<com.example.student.redditpa.TodoItem> TodoItem) {
        currentItem = pos;
        Fragment newFragment = new ItemViewFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();

//        if(findViewById(R.id.detail_fragment_container) == null) {
//            Intent intent = new Intent(getApplicationContext(), TodoActivity.class);
//            startActivity(intent);
//        }
//        else {
//            android.support.v4.app.Fragment detailFragment = TodoActivity.newFragment(TodoItem);
//            getSupportFragmentManager().beginTransaction()
//                    .replace(R.id.detail_fragment_container, detailFragment)
//                    .commit();
//        }
    }
}
