package com.example.student.redditpa;

import android.net.Uri;

import java.util.ArrayList;

public interface ActivityCallback {
    void onPostSelected(int pos, ArrayList<com.example.student.redditpa.TodoItem> TodoItem);
}
