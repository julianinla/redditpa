package com.example.student.redditpa;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class TodoAdapter extends RecyclerView.Adapter<TodoHolder> {

    private ArrayList<com.example.student.redditpa.TodoItem> TodoItem = new ArrayList<>();
    public ActivityCallback activityCallback;

    public TodoAdapter(ArrayList<com.example.student.redditpa.TodoItem> TodoItem, ActivityCallback activityCallback) {
        this.TodoItem = TodoItem;
        this.activityCallback = activityCallback;
    }

    @Override
    public int getItemCount() {
        return TodoItem.size();
    }

    @Override
    public TodoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater.from(parent.getContext()));
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new TodoHolder(view);
    }

    @Override
    public void onBindViewHolder(TodoHolder holder, final int position) {
        holder.titleText.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Log.i("TodoAdapter", "Message: " + TodoItem.get(position).title);
                activityCallback.onPostSelected(position, TodoItem);
            }
        });

        holder.titleText.setText(TodoItem.get(position).title);
    }
}
