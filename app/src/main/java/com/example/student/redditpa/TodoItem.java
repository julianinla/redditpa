package com.example.student.redditpa;

public class TodoItem {
    public String title;
    public String dateadd;
    public String datedue;
    public String category;

    public TodoItem(String title, String dateadd, String datedue, String category) {
        this.title = title;
        this.dateadd = dateadd;
        this.datedue = datedue;
        this.category = category;
    }
}
