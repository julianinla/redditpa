package com.example.student.redditpa;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class TodoListFragment extends Fragment {

    private RecyclerView recyclerView;
    private ActivityCallback activityCallback;
    private TodoActivity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.todolist, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        TodoItem i1 = new TodoItem("Stuff", "Date", "DDate", "Task");
        TodoItem i2 = new TodoItem("Stuff2", "Date2", "DDate2", "Task2");
        TodoItem i3 = new TodoItem("Stuff3", "Date3", "DDate3", "Task3");
        TodoItem i4 = new TodoItem("Stuff4", "Date4", "DDate4", "Task4");

        activity.TodoItem.add(i1);
        activity.TodoItem.add(i2);
        activity.TodoItem.add(i3);
        activity.TodoItem.add(i4);

        TodoAdapter adapter = new TodoAdapter(activity.TodoItem, activityCallback);
        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallback = (ActivityCallback) activity;
        this.activity = (TodoActivity)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityCallback = null;
    }


}
