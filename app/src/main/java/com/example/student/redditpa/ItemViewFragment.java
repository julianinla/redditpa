package com.example.student.redditpa;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ItemViewFragment extends Fragment {

    private ActivityCallback activityCallback;
    private TodoActivity activity;
    private Activity layoutView;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallback = (ActivityCallback) activity;
        this.activity = (TodoActivity)activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutView = inflater.inflate(R.layout.item_fragment, container, false);

        TextView tv1 = (TextView) layoutView.findViewById(R.id.tv1);
        TextView tv2 = (TextView) layoutView.findViewById(R.id.tv2);
        TextView tv3 = (TextView) layoutView.findViewById(R.id.tv3);
        TextView tv4 = (TextView) layoutView.findViewById(R.id.tv4);

        tv1.setText("Name: " + activity.TodoItem.get(activity.currentItem).title);
        tv2.setText("Date Addded: " + activity.TodoItem.get(activity.currentItem).dateadd);
        tv3.setText("Date Due: " + activity.TodoItem.get(activity.currentItem).datedue);
        tv4.setText("Category: " + activity.TodoItem.get(activity.currentItem).category);

        return layoutView;
    }
}
